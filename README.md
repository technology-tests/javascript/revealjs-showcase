# Live demo

The demo link can be found in the project description


# Why reveal.js

## Compatibility

- Only requires a modern web browser
- No display issues such as missing fonts
  (can be included with `@font-face` using CSS),
  or black bars on edges due to different aspect ratios
  (reveal.js uses whole viewport as presentation area by default)

## Looks well as-is

- Plugin support for syntax-highlighted code snippets
  and math typesetting (KaTeX and MathJax)
- Perfectly-aligned shapes, images, and text boxes
- Themes apply to all slides by default
    - Further customizable with CSS

## Easy to use

- Markdown can be used
- Presenter view can be enabled/disabled consistently on the correct screen

## Additional features

- Content is mainly text and can can be paired
  with a version control system such as Git
    - Enables history/small modifications without many copies of the same file
    - Allows distributed collaboration
